հայ էջերի ընտրանի

էջերը պետք է՝
- օգտագործեն [HTTPS պրոտոկոլ](https://hy.wikipedia.org/wiki/HTTPS)
- գրանցված լինեն [.հայ դոմենում](https://hy.wikipedia.org/wiki/.հայ)
- լինեն հայերեն
- լինեն հետաքրքիր